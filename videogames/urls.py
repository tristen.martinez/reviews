from django.urls import path 

from videogames.views import (
    VideoGameListView,
    VideoGameDetailView,
    VideoGameUpdateView,
    VideoGameDeleteView,
    VideoGameCreateView,
    confirmdeletevg,
)

urlpatterns = [
    path("", VideoGameListView.as_view(), name="list_videogames"),
    path("<int:pk>/", VideoGameDetailView.as_view(), name="videogame_detail"),
    path("<int:pk>/edit/", VideoGameUpdateView.as_view(), name="videgame_editreview"),
    path("<int:pk>/delete/", VideoGameDeleteView.as_view(), name="videogame_deletereview"),
    path("new/", VideoGameCreateView.as_view(), name="create_review"),
    path("confirmdelete/videogame", confirmdeletevg, name="confirm_delete_vg"),
]

