from django.db import models

# Create your models here.

#handles our data for video game reviews. 
class Videogamereview(models.Model):
    name = models.CharField(max_length=40)
    studio = models.CharField(max_length=40)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    author_review = models.CharField(max_length=20)
    review = models.TextField(null=True)
    
    def __str__(self):
        return "Title: " + str(self.name) + "Studio: " + str(self.studio)

