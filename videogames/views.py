from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from videogames.models import Videogamereview
from django.urls import reverse_lazy

# Create your views here.


class VideoGameListView(ListView):
    model = Videogamereview
    template_name = "videogames/list.html"
    context_object_name = "vg_list"

class VideoGameDetailView(DetailView):
    model = Videogamereview
    template_name = "videogames/detail.html"
    

class VideoGameUpdateView(UpdateView):
    model = Videogamereview
    template_name = "videogames/edit.html"
    fields = ["name", "studio", "description", "image", "author_review", "review"]

    def get_success_url(self) -> str:
        return reverse_lazy("videogame_detail", args=[self.object.pk])

class VideoGameDeleteView(DeleteView):
    model = Videogamereview
    template_name = "videogames/delete.html"
    success_url = reverse_lazy("confirm_delete_vg")

class VideoGameCreateView(CreateView):
    model = Videogamereview
    template_name = "videogames/new.html"
    fields = ["name", "studio", "description", "image", "author_review", "review"]
    success_url = reverse_lazy("list_videogames")

def confirmdeletevg(request):
    template_name = 'videogames/confirmdelete.html'
    context = {}
    return render(request, template_name, context)





