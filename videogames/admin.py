from django.contrib import admin

# Register your models here.

from videogames.models import Videogamereview

class VideogamereviewAdmin(admin.ModelAdmin):
    pass

admin.site.register(Videogamereview, VideogamereviewAdmin)