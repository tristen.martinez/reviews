from unicodedata import name
from django.db import models

# created a model with the key elements of our page 
class AnimeReview(models.Model):
    name = models.CharField(max_length=40)
    studio = models.CharField(max_length=40)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    author = models.CharField(max_length=20)
    reviews = models.TextField(null=True)

    def __str__(self):
        return "Title: " + str(name) + "studio" + str(self.studio)