from dataclasses import fields
from django.shortcuts import render 
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView    
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
# Create your views here.

from anime.models import (
    AnimeReview,
)



class AnimeReviewListView(ListView):
    model = AnimeReview
    context_object_name = "anime_list"
    template_name = "anime/list.html"
    # fields = ["name", "studio", "description", "image", "author"]

class AnimeReviewDetailView(DetailView):
    model = AnimeReview
    template_name = "anime/detail.html"


class AnimeReviewCreateView(CreateView):
    model = AnimeReview
    template_name = "anime/new.html"
    fields = ["name", "studio", "description", "image", "author", "reviews"] #these are what is under the model 
    success_url = reverse_lazy("anime_list")
    # def get_success_url(self) -> str:
    #     return reverse_lazy("anime_new", args=[self.object.id])

class AnimeReviewDeleteView(DeleteView):
    model = AnimeReview
    template_name = "anime/delete.html"
    success_url = reverse_lazy("confirm_delete_am")

    

class AnimeReviewUpdateView(UpdateView):
    model = AnimeReview
    template_name = "anime/edit.html"
    fields = ["name", "studio", "description", "image", "author", "reviews"] 
    
    def get_success_url(self) -> str:
        return reverse_lazy("anime_detail", args=[self.object.id])
    


def confirmdeleteanime(request):
    template_name = 'anime/confirmdelete.html'
    context = {}
    return render(request, template_name, context)


