from django.contrib import admin
from anime.models import AnimeReview

# Register your models here.

class AnimeReviewAdmin(admin.ModelAdmin):
    pass



admin.site.register(AnimeReview, AnimeReviewAdmin)