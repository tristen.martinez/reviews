from django.urls import path
from anime.views import (
    AnimeReviewListView,
    AnimeReviewDetailView, 
    AnimeReviewCreateView, 
    AnimeReviewDeleteView,
    AnimeReviewUpdateView,
    confirmdeleteanime
    )

urlpatterns = [
path("", AnimeReviewListView.as_view(), name= "anime_list"),
path("<int:pk>/", AnimeReviewDetailView.as_view(), name= "anime_detail"),
path("<int:pk>/edit/", AnimeReviewUpdateView.as_view(), name="anime_edit"),
path("<int:pk>/delete/", AnimeReviewDeleteView.as_view(), name="anime_delete"),
path("new/", AnimeReviewCreateView.as_view(), name="anime_new"),
path("confirmweeb/", confirmdeleteanime, name="confirm_delete_am"),
]