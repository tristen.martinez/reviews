from msilib.schema import ListView
from django.db import models
from re import template
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView  
from django.urls import reverse_lazy
from multiprocessing import context
from django.shortcuts import render
from watchlist.models import AnimeWatchlist
# Create your views here.

class AnimeWatchlistListView(ListView):
    model = AnimeWatchlist
    context_object_name = "watchlist_list"
    template_name = "watchlist/list.html"
    # name = models.ForeignKey(AnimeWatchlist, on_delete = models.CASCADE, related_name = "name")

# class AnimeWatchlistDetailView(DetailView):
#     model = AnimeWatchlist
#     template_name = "watchlist/detail.html"

# class AnimeWatchlistCreateView(CreateView):
#     model = AnimeWatchlist
#     template_name = "watchlist/new.html"
#     fields = ["name", "studio", "image"]
#     success_url = reverse_lazy("watchlist_list")

# class AnimeWatchlistDeleteView(DeleteView):
#     model = AnimeWatchlist
#     template_name = "watchlist/delete.html"
#     success_url = reverse_lazy("watchlist_list")

# class AnimeWatchlistUpdateView(UpdateView):
#     model = AnimeWatchlist
#     template_name = "watchlist/edit.html"
#     fields = ["name", "studio", "image"]

#     def get_success_url(self) -> str:
#         return reverse_lazy("watchlist_detail", args=[self.object.id])

