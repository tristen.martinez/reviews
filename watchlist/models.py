from django.db import models


# created a model with the key elements of our page 
class AnimeWatchlist(models.Model):
    name = models.CharField(max_length=40)
    studio = models.CharField(max_length=40)
    image = models.URLField(null=True, blank=True)

