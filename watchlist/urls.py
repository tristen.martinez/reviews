from django.urls import path
from watchlist.views import (
    AnimeWatchlistListView,
    AnimeWatchlistDetailView,
    AnimeWatchlistCreateView,
    AnimeWatchlistDeleteView,
    AnimeWatchlistUpdateView,
)

urlpatterns = [
path("")
]