from django.contrib import admin
from wishlist.models import WishList

# Register your models here.

class WishListAdmin(admin.ModelAdmin):
    pass 

admin.site.register(WishList, WishListAdmin)




