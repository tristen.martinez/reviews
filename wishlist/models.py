from django.db import models

from videogames.models import Videogamereview

# Create your models here.


class WishList(models.Model):
    name = models.TextField(max_length= 25, null=True)
    videogame = models.ManyToManyField(Videogamereview, related_name="vgtitle")

    def __str__(self):
        return str(self.videogame)

