from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView
from wishlist.models import WishList

# Create your views here.


class WishListListView(ListView):
    model = WishList
    template_name = "wishlist/list.html"
    context_object_name = "wish_list"


class WishListCreateView(CreateView):
    model = WishList
    template_name = "wishlist/new.html"

class WishListDeleteView(DeleteView):
    model = WishList
    template_name = "wishlist/delete.html"
