from django.urls import path

from wishlist.views import (
    WishListListView,
    WishListDeleteView,
)


urlpatterns = [
    path("", WishListListView.as_view(), name="wishlist_url"),
    path("delete/", WishListDeleteView.as_view(), name="delete_wishlist"),
]