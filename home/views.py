from django.shortcuts import render
from django.views.generic.edit import CreateView
from home.models import Birthday
from django.urls import reverse_lazy


def home(request):
    template_name = 'home/home.html'
    context = {}
    return render(request, template_name, context)

# def Agereq(request):
#     template_name = 'home/age.html'
#     context = {}
#     return render(request, template_name, context)

class AgeCheckCreateView(CreateView):
    model = Birthday
    template_name = 'home/age.html'
    fields = ['user_age']

    success_url = reverse_lazy="home"

def AgeFailPage(request):
    template_name = 'home/agefail.html'
    context = {}
    return render(request, template_name, context)

