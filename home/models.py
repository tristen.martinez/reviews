import django
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class Birthday(models.Model):
    user_age = models.SmallIntegerField(validators=[MinValueValidator(18)])
