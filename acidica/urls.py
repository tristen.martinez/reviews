"""acidica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from home.views import home, AgeFailPage, AgeCheckCreateView


urlpatterns = [
    path("", AgeCheckCreateView.as_view(), name='Agecheck'),
    path("videogames/", include("videogames.urls")),
    path("anime/", include("anime.urls")),
    path('admin/', admin.site.urls),
    path('wishlist/', include("wishlist.urls")),
    path("home/", home, name='home'),
    path("BabyWeeb/", AgeFailPage, name='Agefail'),
    
]
